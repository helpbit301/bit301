##Using Git Bash (Command Line)

Please make sure you have downloaded and installed [Git](https://git-scm.com/). 

Git Bash is a command line program for using Git on your machine. The following are commands that you will use to manage your local repository.

#### Set up the local repo ###

You can set up your local machine to act as a repository for your files. 

First navigate to the working directory that you will use as a repository for your files (use ls and cd commands)

* Initialize the local directory as a Git repository

```
$ git init
```

Alternatively, you can **clone** an existing repository.

```
$git clone 
```

* View the current status of your repository (see any changes since the last commit?)

```
$ git status
```


### Ready to commit?? ###
 
A git **commit** means that you want to confirm the changes that you have made in the directory, to all the files or some of the files. Before committing, you will put the files in a **Staging** area.
   
* Stage all files which have changed in the local repository for commit (put in staging area)

```
$ git add .
```

* Stage all, even deleted ones, in the local repository for commit

```
$ git add –A .
```

* Add changes in a particular file to the next commit

```
$ git add -p <file>
```

* Remove a file or files from the staging area 

```
$ git reset 
```


### Committing ###

* Commit the files, which means you are ready to commit the changes you have made.

```
$ git commit -m <Commit Message>
```
### Using remote repository ###

* Add the URL for the remote repository

```
$ git remote add origin <remote repository URL>
```

* View remote repositories

```
$ git view remote -v
```

* Push to the remote repository

```
$ git push origin master
```

* Remove a remote repository

```
$ git remote rm origin
```

### Pulling ###

* pulling from the remote repository: If other people pulled from your remote repository and made their own commits and pushed them, and you want to check what's different from your current set:

```
$ git pull origin master
```

* To see what is different from what we committed, use diff. For the difference to the most recent commit, use  the HEAD pointer.

```
$ git diff HEAD
```

* To see what is different from what we have staged.

```
$ git diff --staged
```

* To change back the status to the last commit

```
$ git checkout -- <target>
```





