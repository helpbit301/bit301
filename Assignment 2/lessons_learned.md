#Lessons Learned Report

**Prepared by:**

	Lee Chong Yik
	Tee Chee Huang
	
**Date:**

	11 November 2015

**Project Name:**

	Wiki Site (Java For-Loop)
	
**Project Sponsor:**

	Ms Ng Shu Min
	
**Project Manager:**

	Lee Chong Yik
	
**Project Dates:**
	
**Final Budget:**

	MYR106,632.00
		
---

1.	Did the project meet scope, time, cost and quality goals?
		
		The project have met the scope as the team is able to complete the project and analyse the risks and success of the project.
		As for time, the project is able to finish in time but the time management for each section for the project did not finish 
		accoridng to its time frame, which result in delay for certain section of the project. For the cost, it is overbudget because 
		the team have to spend more money to cover up the time losses in delay for the early phase of the project. Lastly, the project
		team did a lot of research so that it will met the quality goals but the final result for the quality goals will be receive
		from the feedback of the users to help the team to improve/increase the quality goals to a better state.



2.	What was the success criteria listed in the project scope statement?

		- To complete the project for no more that our estimated budget which is MYR106,632.00.
		- To have the project paying for itself within two years after the wiki site is completed.
		- Have high visits and good feedbacks to meet it’s financial goal.
	


3.	Reflect on whether or not you met the project success criteria.

		Our project met the criteria because the whole project is done within the estimated budget. Moreover, based on the earned value calculated, 
		it is possible that our company will be getting the overall cost for developing this project back within two years.


4.	In terms of managing the project, what were the main lessons your team learned?

		Main lessons learnt in managing the project is that every member has their own roles and responsibility to follow. The project can be 
		completed in the estimated time only if our members practice teamwork with the others. It is important to assign the work and duration 
		to be done evenly to every team members because the project will be delayed if one of the member could not finish his part in time.



5.	Describe one example of what went right on this project.

		The flow of managing and assigning this project is what makes the project to be completed in time. The previous job was done by the 
		time given which will not postpone the job after.


6.	Describe one example of what went wrong on this project.

		The amount of time spent on doing the wiki and calculations on cost and earned value took much more time compare the gantt chart. Thus, the team have to 
		make sure the time spent on the other part of the project be shorten and must be done quicker.




7.	What will you do differently on the next project based on your experience working on this project?

		Based on the experience working on this project, we will be more focused on the quality control of the next project. Although it may 
		cost more in funding the quality control of the project, it gives accreditation to the users which will then give good feedbacks and 
		comments to our project. 



