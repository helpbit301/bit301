##Scope Statement (Version xx)

###Project Title: Educational Wiki Site - For Loop
####Date: 1 September 2015		
####Prepared by: Lee Chong Yik

---

**Project Justification:**

This project is made to assist the company in meeting its strategic goals which is to increase the knowledge of students in Java Programming. The new wiki site will also increase the visibility of the company to students through the wiki site which they will be accessible. 

---

**Product Characteristics and Requirements:**



1. Templates: The site will allow students to download files they can use in their assignments or projects.	
2. Links: All links to other useful sites about Java Programming will be tested before being embeded. Broken links will be removed or fixed after discovery.		
3. Security: The site will must provide several levels of security to ensure that the students are save to access the wiki site and the data in the site are legal and authorized.		
4. Search function: The site must provide a search function for students to search topic by keywords.

---

**Summary of Project Deliverables**
Project management-related deliverables: business case, project charter, scope statement, three sphere model, WBS, gantt chart, progress report, final project presentation, final project report.
Product-related deliverables:

1. A useful educational wiki site.
2. A site which met the student's need.
3. A site managed by administrator.
4. Links included with descriptions for other useful sites.
5. Include templates, downloadable contents, articles, and search function.

---
	
Project Success Criteria: 

Our goal is to complete this project by the beginning of November 2015 for no more than MYR106,632.00. The estimated time for the project paying for itself is within two year after the wiki site is completed. To meet this financial goal, the wiki site must have high visits and good feedbacks. If the project takes longer time to complete or cost more than the estimated cost, the company will still take it as a successful project if it has a good payback.  

---



				
				
				
				
				
				
				
				
				
				
				
				

