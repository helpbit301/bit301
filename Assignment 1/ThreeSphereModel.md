#Three Sphere Model Analysis for Educational WiKi Project - For Loop#

Project Manager : Lee Chong Yik

Member 1 : Tee Chee Huang

Member 2 : Bardia Heshmati

###Introduction###

A wiki is is a collection of user created information about a knowledge area,software or science that is collected from different user inputs .
Educational Wiki is a collabration on underestanding of JAVA language and different areas in JAVA programming language.
Java is a powerful, OS independent programming language that allows developers to design and develop softwares for desktop, mobile and web. these capabilites made java one of the most used and powerful programmin language.

In this part, We are explaining one part of programming language called for-loop, which essentially allows a procees to be done repeatedly based on the number it was given to it.
For-Loop is a key part in any programming language and understanding the core and basics of it in java would help you to use this function in any other programming language.

For Loop is a structrue for a function or multiple functions to run more than once based on the requests. 
For Loop Structure is like below :

for ( start_value; end_value; increment_number ) {

//CODE_HERE

}

start_value : this value usually starts from a minimum number and based on the increment_number reaches to the end_value
end_value : the maximum value that foor loop needs to run a code and exit once it's over this value
increment_number : is the pace that sart_value adds up to reach to the end_value,every time that the code inside the loop is executed, increment_number will be added to start_value 

###Stakeholder Analysis###

*Each team member must identify one stakeholder each and discuss their expectations and how you will manage them*

1. Project Leader. Where they will play a part in planning the projects, supervise and motivate his underlings and also make sure the project is able to reach its objective. They will be expected to spend a great amount of time in meeting with the project sponsor to get the idea of what they want and also meeting with his project members and underlings to see the progress of the project.
2. Wiki Users . Where they will be relying on the information provided on the wiki. Wiki Users are among the important stakeholders. Therefore these information should be accurate and correct to avoid confusion. This wiki should also maintain a good user experiece for fluid navigation and easy access.
3. The Project Team. Where they will assist the project leader to carry out this project. They are expected to brainstorm and provide suggestions to the project leader during the meeting. Their task is to develope the system, user interface design, and database architecture. As a project team, they take the highest responsibility towards the entire project.

###Business Issues###

1. How much would be the cost for hardwares and softwares 
2. How the revenue would be generated from this project
3. How much would Users have to pay for this wiki
4. How much would maintenance and updating cost


###Technology Issues###

1. What platform this wiki would be available in
2. What programming language shall we use to write this wiki with
3. Where would be the server located in
4. How much bandwidth should be allocated to the server
5. What hardwares do we need to develop this Wiki


###Organization Issues###

1. Will this wiki only will be helpful for I.T students or other major can also use it
2. How will it affect students who have already passed their Java Programmin Language
3. How useful and up to date these information would be 
4. Who will be in charge of moderation and updates
5. Do students need specific training to be able to use this wiki 


####Conclusion

Wikis are accessible for all the students and there will be large number of server request for these data, One of the first factors in creating a wiki is to choose a powerful server so that it would be able to handle large amount of request at a time. development of wiki might not take as much time as adding informations to it.
Wikis should have accebtable , accurate and complete information inside them so it would be a must visit page for any students. A programming wiki is not only useful for I.T students, Programming is a power of solving problems and any students who would like to learn how to solve problems can visit this wiki to learn more on how to solve problems.


 
