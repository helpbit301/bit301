##Status/Progress Report

**Project Name: **

Educational WiKi Project - For Loop
 
**Date: **

1 October 2015


**Reporting Period: **

2 October 2015

---
 
####Work completed this reporting period:

Research on Java For-Loop Topic

Gantt Chart, WBS, Three Sphere Model, Project Charter, Scope Statement



####Work to complete next reporting period:

Compilation of all the files

Write a report to evaluate the project


####What’s going well and why:
1. All members doing their part which will help the project to achieve its goal
2. Able to get the issues early three sphere model analysis which is able to have more time to change/improve the project

####What’s not going well and why:
1. Less communication which causes conflicts on work
2. Went off track thus, did not do the work according to the schedule(gantt chart)


####Project changes:
1. Did not follow the work schedule time according to the gantt chart which might cause delay or spendin unnecessary time on a task which need less time to complete
2. Need to get back on track by working quicker on certain parts on the project

####Strategies for meeting project objectives:
1. Brainstorm all the ideas from every member and all members have equal rights to vote which allows the project to have more ways in improving
2. Communication between members needs improvement