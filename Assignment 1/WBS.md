##Work Breakdown Structure for (Java For-Loop)

Prepared by: Tee Chee Huang		Date: 15-9-15	


1.0	Initiating 

	1.1 Three Sphere Model and Shareholder Analysis

	1.2 Prepare Project Charter
	
	1.3 Prepare Business Care
	
	1.4 Hold Project 1st Meeting	

2.0	Planning Task

	2.1 Hold Team 1st Meeting
	
	2.2 Prepare Scope Statement
	
	2.3 Prepare WBS
	
	2.4 Prepare Work Item List
	
	2.5 Prepare schedule and cost baseline
	
		2.5.1 Determine task resources
		
		2.5.2 Determine task durations
		
		2.5.3 Determine task dependencies
		
		2.5.4 Create Baseline Gantt chart
		
3.0	Executing Task
	
	3.1 Research on Java For-Loop
	
	3.2 Prepare all documents related to Loops in java
	
	3.3 Comparison of different loops and for-loop
	
	3.4 Prepare the For-Loop documentation structure
	

4.0 Monitoring and Controlling Task
	
	4.1 Issue Tracking
	
	4.2 Tracking Gantt Chart
	
	4.3 Progress Report
	
5.0	Closing

	5.1 Finalize Documentation
	
	5.2 Prepare Final Project Report
	
	5.3 Prepare Final Project Presentation
	 
	5.4 Lesson Learned
