**Business Case for <Java For-Loop>**

**Date**

2 October 2015

**Prepared by: **

Bardia Heshmati

| **1.0 Introduction/ Background**  | 

	Programming is one of those parts of I.T world that has changed the way we live today, and as we proceed further this urge to learn at least one programming language becomes more and more important. Learning a new programming language needs assitance and a good documemtation to help students visually understand and learn a new programming language. 
	On of these many programming languages , Java is a one of the most used programming language and it has distributed in all platoforms such as desktop , mobile and web . therefore learning java programming language can be very helpful.
	Java is an Object Oriented programming language and it follows the principles of other programming languages like C and C++ . 
	For the purpose of education in this field, we've came up with an idea to create a wiki , a collection of related documentation for this programming language. This wiki can be accecibale online and users can access and contribute to complete these documentations.
	In this part , we are assigned to discuss about one part of java programming language ; Loops and more specifically For-Loops . 
	For-Loop is one of the few loop conditions that allow a bunch of code to bew executed repeatedly.
	This condition is not a new thing in Java programming language but it's a common condition in any programming language. and learning the core concepts of this section would help students to switch between programming language without any hassle.For Loop is a structrue for a function or multiple functions to run more than once based on the requests. For Loop Structure is like below :

	for ( start_value; end_value; increment_number ) {
	//CODE_HERE
	}
	start_value : this value usually starts from a minimum number and based on the increment_number reaches to the end_value end_value : the maximum value that foor loop needs to run a code and exit once it's over this value increment_number : is the pace that sart_value adds up to reach to the end_value,every time that the code inside the loop is executed, increment_number will be added to start_value

--- 
| **2.0 Business Objective**  |
	
	The key reason of creating this business project is to help students and those who are interested to learn java programming language get a good understanding about this programming langauge. It also allows the education institute to promote its educational system .
	It also allows the education institute to generate revenues from selling educational content online through the wiki page.
--- 
| **3.0 Current Situation and Problem/Opportunity Statement**  |

	Many students and programming enthuthiasts tempt to refer to online forums and spend hours on the internet to solve their programming issues , a wiki would allow them to find all their solutions in one place and compare different solutions and read examples online. it also can reccommand different solutions based as they read further in the documentation. 
	A wiki is always moderated by one or group of people to keep all the information up to date and accurate .
	Each page in a wiki can be connected to a video to give more and visual explanation for each section.
--- 
| **4.0 Critical Assumption and Constraints**   |

	Creating a wiki for students to be able to access anytime they face any issues on best assumptions would allow them to get fast answeres to their questions in temrs of programming logic but the wiki only allows them to get their answers in therms of logical problems and not be helpful on complicated and complex case studies. A wiki can be helpful in many cases to help students get their quick answer to any programming syntax and structure but when it comes to complex prblem solving a simple wiki would have its limitations
	
	Assumptions :
	- To provide a full wiki documentation for students who are interested in learning Java.
	- To make this project available online for anyone to use
	- to make it easy for people to contribute to the wiki
	- Wiki to have full documentation on JAVA programming language
	
	Constraints:
	
	- The information to be up to date and accurate all the time
	- The budget allocated for this project to be enough
	- Errors and bugs in the system to be fixed faster
	- Project to be finished before the due date
--- 
| **5.0 Analysis of Option and Recommendation** |

	- To use the current existing system and hardwares in the institute
	- To use free and open source wiki softwares
	- To make it easy for all students and lecturer to contribute on the information
	- Ability to add examples to the wiki that can be used for testing
	
--- 	
| **6.0 Preliminary Project Requirements**     |

	- Project needs to be finished before 6th of november
	- Wiki to be easy to use and easy to access
	- This wiki to be focused on JAVA programming language structures
--- 
| **7.0 Budget Estimate and Financial Analysis**     |

	| Description | Estimated Pricing | 
	|:--------------------------------:|:----------------------:| 
	| Server | RM 10000.00 | 
	| Computers | RM 9000 |
	| Softwares	 | RM 3000 |
	| Development | RM 20000 |
	| Project Team | 12480 |
  
 This project to be finished within 2 months
---
| **8.0 Schedule Estimate**     |

	This Project to be finished and ready for testing phase by November 6th . 
	
	* Due to unexpected hardware of software issues development of the project would be finished 1 week before the estimated time to avoid any clash and passing the due date
---
| **9.0 Potential Risks**     |

	- Hardware failure
	- Software and Internet connectivity
	- Information not be available and up to date all the time
	- Information not being correct or accurate as it mentioned
	- Deadlines are past 
	- Videos uploaded to the page are not easy to watch and download
	
---
| **10.0 Exhibits** Exhibit A: Financial Analysis (Insert image here) | 

http://s21.postimg.org/40ixpvt5z/Screen_Shot_2015_10_02_at_5_19_02_PM.png