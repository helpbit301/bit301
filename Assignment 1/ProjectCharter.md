## Project Charter

**Project Title:** 

Educational WiKi Project - For Loop

**Project Start Date:** 

1 September 2015

**Projected Finish Date:**

6 November 2015

**Budget Information:**

The firm has allocated MYR106,632.00 for this project. The majority of costs for this project will be internal labor. 

**Project Manager:** 

Name: Lee Chong Yik                
E-mail: lcy93@live.com.my              


**Project Objectives:**

To create and manage an educational wiki site about for-loop in Java Programming. The outcome of this project shall enable students to gain more knowledge in Java Programming. Solutions will be addressed from the success criteria outlined below.

**Main Project Success Criteria:**

Constraints:

	- Human power available
	
	- Every progress is done according to gantt chart
	
	- Availability of website for testing

Risks:

	- A problem occurs which doesn't have any solution that meets all the requirements
	
	- No interest from members
	
	- Loss of data
	
	- Increasing of costs, over-budget
	
	- People time may increase (work delayed)


**Approach:**

	Deliverables: 
		- An educational wiki site that is useful
		- Meets the student's requirements
		- Easy to be managed.




**Roles and Responsibilities**

	1. Project Manager: Oversee all project efforts and resource allocation, collect and reporting project metrics.
	
	2. Team Member: Gather resources, publish contents, and research student's needs.
	
| Role | Name | Organization | Position | Email |
|------|------|--------------|----------|-------|
| Project Sponsor | Ng Shu Min     | HELP University  | Lecturer         | ngsm@help.edu.my       |
|   Project Manager   |   Lee Chong Yik   |       HELP University       |     Student     |   lcy93@live.com.my    |
|   Project Team   |   Tee Chee  Huang   |      HELP University        |     Student     |   cheehuangtee@gmail.com    |
|   Project Team   |   Bardia Heshmati   |      HELP University        |     Student     |       |

**Sign-off:** (Signatures of all above stakeholders. Can sign by their names in table above.)

**Comments:** (Handwritten or typed comments from above stakeholders, if applicable)